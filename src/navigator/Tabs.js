import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Screen1 from '../screens/Screen1';
import Screen2 from '../screens/Screen2';
import Screen3 from '../screens/Screen3';
import Screen5 from '../screens/Screen5';

import { Ionicons } from '@expo/vector-icons';

import StackNavigator from './StackNavigator';
import { TourGuideProvider } from 'rn-tourguide';

const Tab = createMaterialBottomTabNavigator();

const Tabs = () => {
    return (
        <TourGuideProvider

            {...{ androidStatusBarVisible: true, preventOutsideInteraction: false }}
        >

            <Tab.Navigator>

                <Tab.Screen
                    name="Screen1"
                    component={Screen1}
                    options={{
                        title: '',
                        tabBarIcon: ({ color }) => (

                            <Ionicons name='ios-add-circle' color={color} size={25} />

                        )

                    }}
                />

                <Tab.Screen
                    name="Screen2"
                    component={Screen2}
                    options={{
                        title: '',
                        tabBarIcon: ({ color }) => (
                            <Ionicons name='ios-chatbubbles' color={color} size={25} />
                        )
                    }}
                />

                <Tab.Screen
                    name="Screen3"
                    component={Screen3}
                    options={{
                        title: '',
                        tabBarIcon: ({ color }) => (
                            <Ionicons name='ios-globe' color={color} size={25} />
                        )
                    }}
                />
                <Tab.Screen
                    name="StackNavigator"
                    component={StackNavigator}
                    options={{
                        title: '',
                        tabBarIcon: ({ color }) => (

                            <Ionicons name='ios-navigate' color={color} size={25} />

                        )
                    }}
                />

                <Tab.Screen
                    name="Screen5"
                    component={Screen5}
                    options={{
                        title: '',
                        tabBarIcon: ({ color }) => (
                            <Ionicons name='ios-rainy' color={color} size={25} />
                        )
                    }}
                />

            </Tab.Navigator>

        </TourGuideProvider>


    )
}

export default Tabs