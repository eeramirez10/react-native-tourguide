import React from 'react'
import 'react-native-gesture-handler';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import Stack1Screen from '../screens/stack/Stack1Screen';
import Stack2Screen from '../screens/stack/Stack2Screen';
import Stack3Screen from '../screens/stack/Stack3Screen';
import { TourGuideProvider } from 'rn-tourguide';

const Stack = createStackNavigator();


const StackNavigator = () => {
    return (

        <TourGuideProvider

            {...{ androidStatusBarVisible: true, preventOutsideInteraction: false }}
        >
            <Stack.Navigator
                screenOptions={{

                    headerTintColor: '#fff',
                    headerStyle: {

                        elevation: 0,
                        backgroundColor: 'rgb(14,127,229)',

                    },
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                }}

            >
                <Stack.Screen name="Stack1Screen" options={{ title: "Pagina 1", }} component={Stack1Screen} />
                <Stack.Screen name="Stack2Screen" options={{ title: "Pagina 2" }} component={Stack2Screen} />
                <Stack.Screen name="Stack3Screen" options={{ title: "Pagina 3" }} component={Stack3Screen} />
            </Stack.Navigator>

        </TourGuideProvider>
    )
}

export default StackNavigator