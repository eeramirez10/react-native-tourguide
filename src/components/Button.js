import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

const Button = ({
    text,
    onPress
}) => {

    return (
        <TouchableOpacity
            style={styles.button}
            onPress={onPress}
        >

            <Text style={styles.buttonText}>{ text }</Text>

        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    button:{
        backgroundColor: '#2980b9',
        paddingVertical: 10,
        paddingHorizontal: 15,
        margin: 2,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },

})

export default Button