import React from 'react'
import {
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import {
    TourGuideZone, // Main wrapper of highlight component
    useTourGuideController, // hook to start, etc.
} from 'rn-tourguide';


const Screen5 = () => {

    const uri = 'https://pbs.twimg.com/profile_images/1223192265969016833/U8AX9Lfn_400x400.jpg';


    return (

        <View style={styles.container}>


            <View style={styles.middleView}>

                <TourGuideZone
                    zone={7}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>
                <TourGuideZone
                    zone={8}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>
                <TourGuideZone
                    zone={9}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>
                <TourGuideZone
                    zone={10}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>
                <TourGuideZone
                    zone={11}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>
                <TourGuideZone
                    zone={12}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>
                <TourGuideZone
                    zone={13}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>

            </View>
        </View>
    )
}

export default Screen5

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 40,
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
    },
    profilePhoto: {
        width: 140,
        height: 140,
        borderRadius: 70,
        marginVertical: 20,
    },
    middleView: {
        flex: 1,
        flexDirection:'row',
        justifyContent:'space-between',
        flexWrap:'wrap',
        paddingHorizontal: 20
     
    },
    button: {
        backgroundColor: '#2980b9',
        paddingVertical: 10,
        paddingHorizontal: 15,
        margin: 2,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },
    row: {
        width: '100%',
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    activeSwitchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
        alignItems: 'center',
        paddingHorizontal: 40,
    },
})