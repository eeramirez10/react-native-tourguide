import { useFocusEffect } from '@react-navigation/native';
import React, { useState, useCallback } from 'react'
import { View, Text, StyleSheet } from 'react-native';
import Button from '../components/Button';



const Screen2 = () => {

    const [count, setCount] = useState(0)

    useFocusEffect(

        useCallback(() => {

            console.log('montado')
           

            let time = setInterval(() => {
                setCount( c => c + 1)
            }, 5000)



            return () => {
                console.log('desmontado')

                clearInterval(time)
            }

        }, [])

    )




    return (
        <View style={styles.container}>
            <Text> Screen 2</Text>
            <Text style={ styles.text }>  { count } </Text>
            
        </View>
    )
}

export default Screen2

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 40,
    },
    text:{
        fontSize:30
    }
})