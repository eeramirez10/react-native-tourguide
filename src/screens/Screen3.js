import React from 'react'
import { View, Text,ScrollView } from 'react-native';

import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-cards';

import { styles } from '../theme/appTheme'

const Screen3 = () => {
    return (
        <View style={{ paddingTop: 40}}>
            
            <ScrollView>

                <Card>
                    <CardImage
                        source={{ uri: 'http://bit.ly/2GfzooV' }}
                        title="Top 10 South African beaches"
                    />
                    <CardTitle
                        subtitle="Number 6"
                    />
                    <CardContent text="Clifton, Western Cape" />
                    <CardAction
                        separator={true}
                        inColumn={false}>
                        <CardButton
                            onPress={() => { }}
                            title="Share"
                            color="#FEB557"
                        />
                        <CardButton
                            onPress={() => { }}
                            title="Explore"
                            color="#FEB557"
                        />
                    </CardAction>
                </Card>

                <Card>
                    <CardImage
                        source={{ uri: 'http://bit.ly/2GfzooV' }}
                        title="Top 10 South African beaches"
                    />
                    <CardTitle
                        subtitle="Number 6"
                    />
                    <CardContent text="Clifton, Western Cape" />
                    <CardAction
                        separator={true}
                        inColumn={false}>
                        <CardButton
                            onPress={() => { }}
                            title="Share"
                            color="#FEB557"
                        />
                        <CardButton
                            onPress={() => { }}
                            title="Explore"
                            color="#FEB557"
                        />
                    </CardAction>
                </Card>
                <Card>
                    <CardImage
                        source={{ uri: 'http://bit.ly/2GfzooV' }}
                        title="Top 10 South African beaches"
                    />
                    <CardTitle
                        subtitle="Number 6"
                    />
                    <CardContent text="Clifton, Western Cape" />
                    <CardAction
                        separator={true}
                        inColumn={false}>
                        <CardButton
                            onPress={() => { }}
                            title="Share"
                            color="#FEB557"
                        />
                        <CardButton
                            onPress={() => { }}
                            title="Explore"
                            color="#FEB557"
                        />
                    </CardAction>
                </Card>

            </ScrollView>
        </View>
    )
}

export default Screen3