import React, { useEffect } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Button, StyleSheet } from 'react-native'

import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-cards';


import {
  TourGuideZone,
  useTourGuideController,
} from 'rn-tourguide'


const Stack1Screen = ({ navigation }) => {

  const { start, canStart } = useTourGuideController();

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <TouchableOpacity
            style={styles.buttonTour}
            onPress={() => start()}
          >

            <Text style={{ color: 'white' }}>Empezar tour</Text>

          </TouchableOpacity>
        )
      }
    })



  }, [canStart])



  return (

    <View >

      <ScrollView
        showsVerticalScrollIndicator={false}
      >


        <TourGuideZone
          zone={1}
          shape='rectangle'
          text={'Card 1'}
        >

          <Card >
            <CardImage
              source={{ uri: 'http://bit.ly/2GfzooV' }}
              title="Top 10 South African beaches"
            />
            <CardTitle
              subtitle="Number 6"
            />
            <CardContent text="Clifton, Western Cape" />
            <CardAction
              separator={true}
              inColumn={false}>
              <CardButton
                onPress={() => { }}
                title="Share"
                color="#FEB557"
              />
              <CardButton
                onPress={() => navigation.navigate('Stack2Screen')}
                title="Explore"
                color="#FEB557"
              />
            </CardAction>
          </Card>

        </TourGuideZone>

        <TourGuideZone
          zone={2}
          shape='rectangle'
          text={'Card 2'}

        >

          <Card>
            <CardImage
              source={{ uri: 'http://bit.ly/2GfzooV' }}
              title="Top 10 South African beaches"
            />
            <CardTitle
              subtitle="Number 6"
            />
            <CardContent text="Clifton, Western Cape" />
            <CardAction
              separator={true}
              inColumn={false}>
              <CardButton
                onPress={() => { }}
                title="Share"
                color="#FEB557"
              />
              <CardButton
                onPress={() => navigation.navigate('Stack2Screen')}
                title="Explore"
                color="#FEB557"
              />
            </CardAction>
          </Card>


        </TourGuideZone>



      </ScrollView>


    </View>






  )
}

const styles = StyleSheet.create({
  buttonTour: {
    backgroundColor: '#5856D6',
    padding: 10,
    alignItems: 'center',
    marginRight: 10

  }
})

export default Stack1Screen

