import React  from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { styles } from '../../theme/appTheme'


const Stack2Screen = ({ navigation }) => {

    return (
        <View style={styles.globalMargin}>
            <Text> Stack2Screen </Text>

            <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate('Stack3Screen')}
            >

                <Text style={{ color: 'white' }}>Ir a pagina 3</Text>

            </TouchableOpacity>
        </View>
    )
}

export default Stack2Screen

