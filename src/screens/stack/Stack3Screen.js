import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import { styles } from '../../theme/appTheme'

import {HeaderBackButton} from '@react-navigation/elements';

const Stack3Screen = ({ navigation }) => {

    useEffect(() => {
        navigation.setOptions({
            headerLeft: (props) => {
                return <HeaderBackButton {...props}  onPress={() => navigation.popToTop()} />
            }
        })
    }, []);


    return (
        <View style={styles.globalMargin}>
            <Text> Stack3Screen </Text>
        </View>
    )
}

export default Stack3Screen

