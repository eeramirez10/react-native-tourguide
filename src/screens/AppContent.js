import React from 'react';

import { Ionicons } from '@expo/vector-icons'

import {
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native'

import {
 
    TourGuideZone, // Main wrapper of highlight component

    useTourGuideController, // hook to start, etc.
} from 'rn-tourguide'

import Button from '../components/Button';

const AppContent = () => {

    const uri = 'https://pbs.twimg.com/profile_images/1223192265969016833/U8AX9Lfn_400x400.jpg';

    const iconProps = { size: 40, color: '#888' }

    const { start, canStart, stop, eventEmitter } = useTourGuideController();

    React.useEffect(() => {
        // start at mount
        if (canStart) {
            start()
        }
    }, [canStart]) 


    return (

        <View style={styles.container}>
        

            <TourGuideZone
                zone={2}
                text={'A react-native-copilot remastered! 🎉'}
                borderRadius={16}
            >
                <Text style={styles.title}>
                    {'Welcome to the demo of\n"rn-tourguide"'}
                </Text>

            </TourGuideZone>

            <View style={styles.middleView}>

                <Button text='START THE TUTORIAL!' onPress={() => start()} />

                <TourGuideZone zone={3} shape={'rectangle_and_keep'}>
                    <Button text="Step 4" onPress={() => start(4)} />
                </TourGuideZone>

                <Button text="Step 2" onPress={() => start(2)}/>

                <Button text="Stop" onPress={() => stop()} />

                <TourGuideZone
                    zone={1}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />
                    <Image source={{ uri }} style={styles.profilePhoto} />
                </TourGuideZone>

            </View>

            <View style={styles.row}>

                <TourGuideZone zone={4} shape={'circle'} tooltipBottomOffset={200}>
                    <Ionicons name='ios-add-circle' {...iconProps} />
                </TourGuideZone>
                <Ionicons name='ios-chatbubbles' {...iconProps} />
                <Ionicons name='ios-globe' {...iconProps} />
                <TourGuideZone zone={5}>
                    <Ionicons name='ios-navigate' {...iconProps} />
                </TourGuideZone>
                <TourGuideZone zone={6} shape={'circle'}>
                    <Ionicons name='ios-rainy' {...iconProps} />
                </TourGuideZone>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 40,
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
    },
    profilePhoto: {
        width: 140,
        height: 140,
        borderRadius: 70,
        marginVertical: 20,
    },
    middleView: {
        flex: 1,
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#2980b9',
        paddingVertical: 10,
        paddingHorizontal: 15,
        margin: 2,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },
    row: {
        width: '100%',
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    activeSwitchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
        alignItems: 'center',
        paddingHorizontal: 40,
    },
})

export default AppContent