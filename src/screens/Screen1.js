import React from 'react'
import {
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native'

import {
    TourGuideZone, 
    useTourGuideController, 
} from 'rn-tourguide'

import Button from '../components/Button';

const Screen1 = () => {
    const uri = 'https://pbs.twimg.com/profile_images/1223192265969016833/U8AX9Lfn_400x400.jpg';

    const { start, stop, eventEmitter } = useTourGuideController();

    React.useEffect(() => {
        
        eventEmitter.on('stop', () => console.log('stop'))

        eventEmitter.on('stepChange', step => {
            const s = step ? step.order:0
        })
    
        return () => {
            eventEmitter.off('stop', null)
        }


    }, [])


    return (

        <View style={styles.container}>


            <TourGuideZone
                zone={2}
                text={'A react-native-copilot remastered! 🎉'}
                borderRadius={16}
            >
                <Text style={styles.title}>
                    {'Welcome to the demo of\n"rn-tourguide"'}
                </Text>

            </TourGuideZone>

            <View style={styles.middleView}>

                <Button text='START THE TUTORIAL!' onPress={() => start(1)} />

                <TourGuideZone zone={3} shape={'rectangle_and_keep'}>
                    <Button text="Step 4" onPress={() => start(4)} />
                </TourGuideZone>

                <Button text="Step 2" onPress={() => start(2)} />

                <Button text="Stop" onPress={() => stop()} />

                <TourGuideZone
                    zone={1}
                    shape='circle'
                    text={'With animated SVG morphing with awesome flubber 🍮💯'}
                >
                    <Image source={{ uri }} style={styles.profilePhoto} />

                </TourGuideZone>

            </View>

        </View>


    )
}

export default Screen1

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 40,
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
    },
    profilePhoto: {
        width: 140,
        height: 140,
        borderRadius: 70,
        marginVertical: 20,
    },
    middleView: {
        flex: 1,
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#2980b9',
        paddingVertical: 10,
        paddingHorizontal: 15,
        margin: 2,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },
    row: {
        width: '100%',
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    activeSwitchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
        alignItems: 'center',
        paddingHorizontal: 40,
    },
})