import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 40,
    },
    globalMargin: {
        marginHorizontal:20,
        flex: 1
    },
    title:{
        fontSize:30,
        marginBottom:10
    },
    botonGrande:{
        width:100,
        height:100,
        backgroundColor:'red',
        borderRadius:20,
        alignContent:'center',
        justifyContent:'center',
        alignItems:'center',
        margin: 10 
    },
    botonGrandeTexto:{
        color:'white',
        fontSize:18,
        fontWeight: 'bold'
    },
    avater:{
        width:150,
        height:150,
        borderRadius:100
    },
    viewImage:{
        alignItems:'center',
        marginTop:20
    },
    menuContainer:{
        marginVertical:50,
        marginHorizontal: 50,
       
    },
    menuTexto:{
        fontSize:20,
        
    },
    menuButton:{
        marginBottom: 20
    },
    button:{
        
        backgroundColor: '#5856D6',
        padding: 10,
        alignItems:'center',
        zIndex:99
        
        
    }

})