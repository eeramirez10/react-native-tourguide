import { StatusBar } from 'expo-status-bar';
import Tabs from './src/navigator/Tabs';
import { NavigationContainer } from '@react-navigation/native';

export default function App() {
  return (

    <NavigationContainer >
        <StatusBar />
        <Tabs />
    </NavigationContainer>


  );
}


